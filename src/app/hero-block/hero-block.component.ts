import { Component, OnInit, AfterViewInit } from '@angular/core';
import Typed from 'typed.js';

@Component({
  selector: 'sd-hero-block',
  templateUrl: './hero-block.component.html',
  styleUrls: ['./hero-block.component.scss']
})
export class HeroBlockComponent implements OnInit, AfterViewInit {
  typed;

  constructor() { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.typed = new Typed('.js-typed',{
      stringsElement: '.js-typed-strings',
      typeSpeed: 60,
      startDelay: 50,
      backSpeed: 30,
      backDelay: 3950,
      loop: true,
      showCursor: false
    });
  }
}
