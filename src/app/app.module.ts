import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopAppBarComponent } from './top-app-bar/top-app-bar.component';
import { HeroBlockComponent } from './hero-block/hero-block.component';

@NgModule({
  declarations: [
    AppComponent,
    TopAppBarComponent,
    HeroBlockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
