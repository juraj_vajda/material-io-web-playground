import { Component, OnInit, AfterViewInit } from '@angular/core';

import { MDCRipple } from '@material/ripple';

@Component({
  selector: 'sd-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, AfterViewInit {
  title = 'material-io-web-playground';
  selector = '.mdc-button, .mdc-icon-button, .mdc-card__primary-action';

  constructor() { }

  ngOnInit() { }

  ngAfterViewInit() {
    [].map.call(document.querySelectorAll(this.selector), function(el) {
      return MDCRipple.attachTo(el);
    });
  }
}
