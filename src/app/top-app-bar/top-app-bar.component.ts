import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MDCTopAppBar } from '@material/top-app-bar';

@Component({
  selector: 'sd-top-app-bar',
  templateUrl: './top-app-bar.component.html',
  styleUrls: ['./top-app-bar.component.scss']
})
export class TopAppBarComponent implements OnInit, AfterViewInit {
  topAppBarElement;
  topAppBar;

  constructor() { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    this.topAppBarElement = document.querySelector('.mdc-top-app-bar');
    this.topAppBar = new MDCTopAppBar(this.topAppBarElement);
  }
}
